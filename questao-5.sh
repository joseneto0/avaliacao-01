#!/bin/bash

echo "Variaveis automáticas do sistema: "
echo "\$HOME:"
echo $HOME
echo "\$PWD:"
echo $PWD
echo "\$PATH:"
echo $PATH
echo "\$USER:"
echo $USER
echo "\$SHELL:"
echo $SHELL
echo "\$UID:"
echo $UID
echo "\$LANG:"
echo $LANG
echo "\$RANDOM:"
echo $RANDOM
echo "\$DISPLAY:"
echo $DISPLAY
echo "\$OSTYPE:"
echo $OSTYPE
